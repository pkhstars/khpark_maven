import static org.junit.Assert.*;
import org.junit.Test;
public class Sample1Test {
	@Test
   public void testReverse() {
       String actual = Sample1.reverse("hello");
       String expected = "olleh";
       assertEquals(expected, actual);
    }
}